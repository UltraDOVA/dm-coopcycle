package com.mycompany.store.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.mycompany.store.domain.enumeration.Role;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Utilisateur.
 */
@Entity
@Table(name = "utilisateur")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Utilisateur implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "nom")
    private String nom;

    @Column(name = "mail")
    private String mail;

    @Column(name = "tel")
    private String tel;

    @Enumerated(EnumType.STRING)
    @Column(name = "role")
    private Role role;

    @OneToMany(mappedBy = "utilisateur")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "coursiers", "produits", "utilisateur" }, allowSetters = true)
    private Set<Panier> paniers = new HashSet<>();

    @OneToMany(mappedBy = "utilisateur")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "utilisateur", "panier" }, allowSetters = true)
    private Set<Coursier> coursiers = new HashSet<>();

    @OneToMany(mappedBy = "utilisateur")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "produits", "cooperatives", "utilisateur" }, allowSetters = true)
    private Set<Boutique> boutiques = new HashSet<>();

    @ManyToMany
    @JoinTable(
        name = "rel_utilisateur__cooperative",
        joinColumns = @JoinColumn(name = "utilisateur_id"),
        inverseJoinColumns = @JoinColumn(name = "cooperative_id")
    )
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "utilisateurs", "boutiques" }, allowSetters = true)
    private Set<Cooperative> cooperatives = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Utilisateur id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return this.nom;
    }

    public Utilisateur nom(String nom) {
        this.setNom(nom);
        return this;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getMail() {
        return this.mail;
    }

    public Utilisateur mail(String mail) {
        this.setMail(mail);
        return this;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getTel() {
        return this.tel;
    }

    public Utilisateur tel(String tel) {
        this.setTel(tel);
        return this;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public Role getRole() {
        return this.role;
    }

    public Utilisateur role(Role role) {
        this.setRole(role);
        return this;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Set<Panier> getPaniers() {
        return this.paniers;
    }

    public void setPaniers(Set<Panier> paniers) {
        if (this.paniers != null) {
            this.paniers.forEach(i -> i.setUtilisateur(null));
        }
        if (paniers != null) {
            paniers.forEach(i -> i.setUtilisateur(this));
        }
        this.paniers = paniers;
    }

    public Utilisateur paniers(Set<Panier> paniers) {
        this.setPaniers(paniers);
        return this;
    }

    public Utilisateur addPanier(Panier panier) {
        this.paniers.add(panier);
        panier.setUtilisateur(this);
        return this;
    }

    public Utilisateur removePanier(Panier panier) {
        this.paniers.remove(panier);
        panier.setUtilisateur(null);
        return this;
    }

    public Set<Coursier> getCoursiers() {
        return this.coursiers;
    }

    public void setCoursiers(Set<Coursier> coursiers) {
        if (this.coursiers != null) {
            this.coursiers.forEach(i -> i.setUtilisateur(null));
        }
        if (coursiers != null) {
            coursiers.forEach(i -> i.setUtilisateur(this));
        }
        this.coursiers = coursiers;
    }

    public Utilisateur coursiers(Set<Coursier> coursiers) {
        this.setCoursiers(coursiers);
        return this;
    }

    public Utilisateur addCoursier(Coursier coursier) {
        this.coursiers.add(coursier);
        coursier.setUtilisateur(this);
        return this;
    }

    public Utilisateur removeCoursier(Coursier coursier) {
        this.coursiers.remove(coursier);
        coursier.setUtilisateur(null);
        return this;
    }

    public Set<Boutique> getBoutiques() {
        return this.boutiques;
    }

    public void setBoutiques(Set<Boutique> boutiques) {
        if (this.boutiques != null) {
            this.boutiques.forEach(i -> i.setUtilisateur(null));
        }
        if (boutiques != null) {
            boutiques.forEach(i -> i.setUtilisateur(this));
        }
        this.boutiques = boutiques;
    }

    public Utilisateur boutiques(Set<Boutique> boutiques) {
        this.setBoutiques(boutiques);
        return this;
    }

    public Utilisateur addBoutique(Boutique boutique) {
        this.boutiques.add(boutique);
        boutique.setUtilisateur(this);
        return this;
    }

    public Utilisateur removeBoutique(Boutique boutique) {
        this.boutiques.remove(boutique);
        boutique.setUtilisateur(null);
        return this;
    }

    public Set<Cooperative> getCooperatives() {
        return this.cooperatives;
    }

    public void setCooperatives(Set<Cooperative> cooperatives) {
        this.cooperatives = cooperatives;
    }

    public Utilisateur cooperatives(Set<Cooperative> cooperatives) {
        this.setCooperatives(cooperatives);
        return this;
    }

    public Utilisateur addCooperative(Cooperative cooperative) {
        this.cooperatives.add(cooperative);
        cooperative.getUtilisateurs().add(this);
        return this;
    }

    public Utilisateur removeCooperative(Cooperative cooperative) {
        this.cooperatives.remove(cooperative);
        cooperative.getUtilisateurs().remove(this);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Utilisateur)) {
            return false;
        }
        return id != null && id.equals(((Utilisateur) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Utilisateur{" +
            "id=" + getId() +
            ", nom='" + getNom() + "'" +
            ", mail='" + getMail() + "'" +
            ", tel='" + getTel() + "'" +
            ", role='" + getRole() + "'" +
            "}";
    }
}
