package com.mycompany.store.domain.enumeration;

/**
 * The Role enumeration.
 */
public enum Role {
    Client,
    Societaire,
    DirecteurGeneral,
    Commercant,
    Coursier,
}
