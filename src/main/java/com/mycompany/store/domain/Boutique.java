package com.mycompany.store.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Boutique.
 */
@Entity
@Table(name = "boutique")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Boutique implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy = "boutique")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "boutique", "paniers" }, allowSetters = true)
    private Set<Produit> produits = new HashSet<>();

    @ManyToMany
    @JoinTable(
        name = "rel_boutique__cooperative",
        joinColumns = @JoinColumn(name = "boutique_id"),
        inverseJoinColumns = @JoinColumn(name = "cooperative_id")
    )
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "utilisateurs", "boutiques" }, allowSetters = true)
    private Set<Cooperative> cooperatives = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = { "paniers", "coursiers", "boutiques", "cooperatives" }, allowSetters = true)
    private Utilisateur utilisateur;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Boutique id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public Boutique name(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Produit> getProduits() {
        return this.produits;
    }

    public void setProduits(Set<Produit> produits) {
        if (this.produits != null) {
            this.produits.forEach(i -> i.setBoutique(null));
        }
        if (produits != null) {
            produits.forEach(i -> i.setBoutique(this));
        }
        this.produits = produits;
    }

    public Boutique produits(Set<Produit> produits) {
        this.setProduits(produits);
        return this;
    }

    public Boutique addProduit(Produit produit) {
        this.produits.add(produit);
        produit.setBoutique(this);
        return this;
    }

    public Boutique removeProduit(Produit produit) {
        this.produits.remove(produit);
        produit.setBoutique(null);
        return this;
    }

    public Set<Cooperative> getCooperatives() {
        return this.cooperatives;
    }

    public void setCooperatives(Set<Cooperative> cooperatives) {
        this.cooperatives = cooperatives;
    }

    public Boutique cooperatives(Set<Cooperative> cooperatives) {
        this.setCooperatives(cooperatives);
        return this;
    }

    public Boutique addCooperative(Cooperative cooperative) {
        this.cooperatives.add(cooperative);
        cooperative.getBoutiques().add(this);
        return this;
    }

    public Boutique removeCooperative(Cooperative cooperative) {
        this.cooperatives.remove(cooperative);
        cooperative.getBoutiques().remove(this);
        return this;
    }

    public Utilisateur getUtilisateur() {
        return this.utilisateur;
    }

    public void setUtilisateur(Utilisateur utilisateur) {
        this.utilisateur = utilisateur;
    }

    public Boutique utilisateur(Utilisateur utilisateur) {
        this.setUtilisateur(utilisateur);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Boutique)) {
            return false;
        }
        return id != null && id.equals(((Boutique) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Boutique{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            "}";
    }
}
