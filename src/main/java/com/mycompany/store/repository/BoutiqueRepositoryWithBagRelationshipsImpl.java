package com.mycompany.store.repository;

import com.mycompany.store.domain.Boutique;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.stream.IntStream;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hibernate.annotations.QueryHints;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

/**
 * Utility repository to load bag relationships based on https://vladmihalcea.com/hibernate-multiplebagfetchexception/
 */
public class BoutiqueRepositoryWithBagRelationshipsImpl implements BoutiqueRepositoryWithBagRelationships {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Optional<Boutique> fetchBagRelationships(Optional<Boutique> boutique) {
        return boutique.map(this::fetchCooperatives);
    }

    @Override
    public Page<Boutique> fetchBagRelationships(Page<Boutique> boutiques) {
        return new PageImpl<>(fetchBagRelationships(boutiques.getContent()), boutiques.getPageable(), boutiques.getTotalElements());
    }

    @Override
    public List<Boutique> fetchBagRelationships(List<Boutique> boutiques) {
        return Optional.of(boutiques).map(this::fetchCooperatives).orElse(Collections.emptyList());
    }

    Boutique fetchCooperatives(Boutique result) {
        return entityManager
            .createQuery(
                "select boutique from Boutique boutique left join fetch boutique.cooperatives where boutique is :boutique",
                Boutique.class
            )
            .setParameter("boutique", result)
            .setHint(QueryHints.PASS_DISTINCT_THROUGH, false)
            .getSingleResult();
    }

    List<Boutique> fetchCooperatives(List<Boutique> boutiques) {
        HashMap<Object, Integer> order = new HashMap<>();
        IntStream.range(0, boutiques.size()).forEach(index -> order.put(boutiques.get(index).getId(), index));
        List<Boutique> result = entityManager
            .createQuery(
                "select distinct boutique from Boutique boutique left join fetch boutique.cooperatives where boutique in :boutiques",
                Boutique.class
            )
            .setParameter("boutiques", boutiques)
            .setHint(QueryHints.PASS_DISTINCT_THROUGH, false)
            .getResultList();
        Collections.sort(result, (o1, o2) -> Integer.compare(order.get(o1.getId()), order.get(o2.getId())));
        return result;
    }
}
