package com.mycompany.store.repository;

import com.mycompany.store.domain.Panier;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;

public interface PanierRepositoryWithBagRelationships {
    Optional<Panier> fetchBagRelationships(Optional<Panier> panier);

    List<Panier> fetchBagRelationships(List<Panier> paniers);

    Page<Panier> fetchBagRelationships(Page<Panier> paniers);
}
