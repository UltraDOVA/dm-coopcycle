package com.mycompany.store.repository;

import com.mycompany.store.domain.Boutique;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;

public interface BoutiqueRepositoryWithBagRelationships {
    Optional<Boutique> fetchBagRelationships(Optional<Boutique> boutique);

    List<Boutique> fetchBagRelationships(List<Boutique> boutiques);

    Page<Boutique> fetchBagRelationships(Page<Boutique> boutiques);
}
