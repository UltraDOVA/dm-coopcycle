import { IUtilisateur } from '@/shared/model/utilisateur.model';
import { IPanier } from '@/shared/model/panier.model';

export interface ICoursier {
  id?: number;
  utilisateur?: IUtilisateur | null;
  panier?: IPanier | null;
}

export class Coursier implements ICoursier {
  constructor(public id?: number, public utilisateur?: IUtilisateur | null, public panier?: IPanier | null) {}
}
