import { IUtilisateur } from '@/shared/model/utilisateur.model';
import { IBoutique } from '@/shared/model/boutique.model';

export interface ICooperative {
  id?: number;
  name?: string | null;
  utilisateur?: string | null;
  directeurGeneral?: string | null;
  societaire?: string | null;
  cA?: string | null;
  utilisateurs?: IUtilisateur[] | null;
  boutiques?: IBoutique[] | null;
}

export class Cooperative implements ICooperative {
  constructor(
    public id?: number,
    public name?: string | null,
    public utilisateur?: string | null,
    public directeurGeneral?: string | null,
    public societaire?: string | null,
    public cA?: string | null,
    public utilisateurs?: IUtilisateur[] | null,
    public boutiques?: IBoutique[] | null
  ) {}
}
