import { ICoursier } from '@/shared/model/coursier.model';
import { IProduit } from '@/shared/model/produit.model';
import { IUtilisateur } from '@/shared/model/utilisateur.model';

export interface IPanier {
  id?: number;
  coursiers?: ICoursier[] | null;
  produits?: IProduit[] | null;
  utilisateur?: IUtilisateur | null;
}

export class Panier implements IPanier {
  constructor(
    public id?: number,
    public coursiers?: ICoursier[] | null,
    public produits?: IProduit[] | null,
    public utilisateur?: IUtilisateur | null
  ) {}
}
