export enum Role {
  Client = 'Client',

  Societaire = 'Societaire',

  DirecteurGeneral = 'DirecteurGeneral',

  Commercant = 'Commercant',

  Coursier = 'Coursier',
}
