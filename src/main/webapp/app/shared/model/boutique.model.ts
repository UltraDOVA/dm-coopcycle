import { IProduit } from '@/shared/model/produit.model';
import { ICooperative } from '@/shared/model/cooperative.model';
import { IUtilisateur } from '@/shared/model/utilisateur.model';

export interface IBoutique {
  id?: number;
  name?: string | null;
  produits?: IProduit[] | null;
  cooperatives?: ICooperative[] | null;
  utilisateur?: IUtilisateur | null;
}

export class Boutique implements IBoutique {
  constructor(
    public id?: number,
    public name?: string | null,
    public produits?: IProduit[] | null,
    public cooperatives?: ICooperative[] | null,
    public utilisateur?: IUtilisateur | null
  ) {}
}
