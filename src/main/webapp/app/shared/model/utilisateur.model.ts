import { IPanier } from '@/shared/model/panier.model';
import { ICoursier } from '@/shared/model/coursier.model';
import { IBoutique } from '@/shared/model/boutique.model';
import { ICooperative } from '@/shared/model/cooperative.model';

import { Role } from '@/shared/model/enumerations/role.model';
export interface IUtilisateur {
  id?: number;
  nom?: string | null;
  mail?: string | null;
  tel?: string | null;
  role?: Role | null;
  paniers?: IPanier[] | null;
  coursiers?: ICoursier[] | null;
  boutiques?: IBoutique[] | null;
  cooperatives?: ICooperative[] | null;
}

export class Utilisateur implements IUtilisateur {
  constructor(
    public id?: number,
    public nom?: string | null,
    public mail?: string | null,
    public tel?: string | null,
    public role?: Role | null,
    public paniers?: IPanier[] | null,
    public coursiers?: ICoursier[] | null,
    public boutiques?: IBoutique[] | null,
    public cooperatives?: ICooperative[] | null
  ) {}
}
