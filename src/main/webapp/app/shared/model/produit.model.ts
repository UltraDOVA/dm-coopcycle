import { IBoutique } from '@/shared/model/boutique.model';
import { IPanier } from '@/shared/model/panier.model';

export interface IProduit {
  id?: number;
  name?: string | null;
  boutique?: IBoutique | null;
  paniers?: IPanier[] | null;
}

export class Produit implements IProduit {
  constructor(public id?: number, public name?: string | null, public boutique?: IBoutique | null, public paniers?: IPanier[] | null) {}
}
