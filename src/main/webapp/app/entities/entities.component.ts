import { Component, Provide, Vue } from 'vue-property-decorator';

import UserService from '@/entities/user/user.service';
import UtilisateurService from './utilisateur/utilisateur.service';
import CoursierService from './coursier/coursier.service';
import PanierService from './panier/panier.service';
import ProduitService from './produit/produit.service';
import BoutiqueService from './boutique/boutique.service';
import CooperativeService from './cooperative/cooperative.service';
// jhipster-needle-add-entity-service-to-entities-component-import - JHipster will import entities services here

@Component
export default class Entities extends Vue {
  @Provide('userService') private userService = () => new UserService();
  @Provide('utilisateurService') private utilisateurService = () => new UtilisateurService();
  @Provide('coursierService') private coursierService = () => new CoursierService();
  @Provide('panierService') private panierService = () => new PanierService();
  @Provide('produitService') private produitService = () => new ProduitService();
  @Provide('boutiqueService') private boutiqueService = () => new BoutiqueService();
  @Provide('cooperativeService') private cooperativeService = () => new CooperativeService();
  // jhipster-needle-add-entity-service-to-entities-component - JHipster will import entities services here
}
