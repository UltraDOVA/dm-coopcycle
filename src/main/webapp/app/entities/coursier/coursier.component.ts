import { Component, Vue, Inject } from 'vue-property-decorator';
import Vue2Filters from 'vue2-filters';
import { ICoursier } from '@/shared/model/coursier.model';

import CoursierService from './coursier.service';
import AlertService from '@/shared/alert/alert.service';

@Component({
  mixins: [Vue2Filters.mixin],
})
export default class Coursier extends Vue {
  @Inject('coursierService') private coursierService: () => CoursierService;
  @Inject('alertService') private alertService: () => AlertService;

  private removeId: number = null;

  public coursiers: ICoursier[] = [];

  public isFetching = false;

  public mounted(): void {
    this.retrieveAllCoursiers();
  }

  public clear(): void {
    this.retrieveAllCoursiers();
  }

  public retrieveAllCoursiers(): void {
    this.isFetching = true;
    this.coursierService()
      .retrieve()
      .then(
        res => {
          this.coursiers = res.data;
          this.isFetching = false;
        },
        err => {
          this.isFetching = false;
          this.alertService().showHttpError(this, err.response);
        }
      );
  }

  public handleSyncList(): void {
    this.clear();
  }

  public prepareRemove(instance: ICoursier): void {
    this.removeId = instance.id;
    if (<any>this.$refs.removeEntity) {
      (<any>this.$refs.removeEntity).show();
    }
  }

  public removeCoursier(): void {
    this.coursierService()
      .delete(this.removeId)
      .then(() => {
        const message = this.$t('storeApp.coursier.deleted', { param: this.removeId });
        this.$bvToast.toast(message.toString(), {
          toaster: 'b-toaster-top-center',
          title: 'Info',
          variant: 'danger',
          solid: true,
          autoHideDelay: 5000,
        });
        this.removeId = null;
        this.retrieveAllCoursiers();
        this.closeDialog();
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public closeDialog(): void {
    (<any>this.$refs.removeEntity).hide();
  }
}
