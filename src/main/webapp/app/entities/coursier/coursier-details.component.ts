import { Component, Vue, Inject } from 'vue-property-decorator';

import { ICoursier } from '@/shared/model/coursier.model';
import CoursierService from './coursier.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class CoursierDetails extends Vue {
  @Inject('coursierService') private coursierService: () => CoursierService;
  @Inject('alertService') private alertService: () => AlertService;

  public coursier: ICoursier = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.coursierId) {
        vm.retrieveCoursier(to.params.coursierId);
      }
    });
  }

  public retrieveCoursier(coursierId) {
    this.coursierService()
      .find(coursierId)
      .then(res => {
        this.coursier = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
