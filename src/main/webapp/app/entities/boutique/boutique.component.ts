import { Component, Vue, Inject } from 'vue-property-decorator';
import Vue2Filters from 'vue2-filters';
import { IBoutique } from '@/shared/model/boutique.model';

import BoutiqueService from './boutique.service';
import AlertService from '@/shared/alert/alert.service';

@Component({
  mixins: [Vue2Filters.mixin],
})
export default class Boutique extends Vue {
  @Inject('boutiqueService') private boutiqueService: () => BoutiqueService;
  @Inject('alertService') private alertService: () => AlertService;

  private removeId: number = null;

  public boutiques: IBoutique[] = [];

  public isFetching = false;

  public mounted(): void {
    this.retrieveAllBoutiques();
  }

  public clear(): void {
    this.retrieveAllBoutiques();
  }

  public retrieveAllBoutiques(): void {
    this.isFetching = true;
    this.boutiqueService()
      .retrieve()
      .then(
        res => {
          this.boutiques = res.data;
          this.isFetching = false;
        },
        err => {
          this.isFetching = false;
          this.alertService().showHttpError(this, err.response);
        }
      );
  }

  public handleSyncList(): void {
    this.clear();
  }

  public prepareRemove(instance: IBoutique): void {
    this.removeId = instance.id;
    if (<any>this.$refs.removeEntity) {
      (<any>this.$refs.removeEntity).show();
    }
  }

  public removeBoutique(): void {
    this.boutiqueService()
      .delete(this.removeId)
      .then(() => {
        const message = this.$t('storeApp.boutique.deleted', { param: this.removeId });
        this.$bvToast.toast(message.toString(), {
          toaster: 'b-toaster-top-center',
          title: 'Info',
          variant: 'danger',
          solid: true,
          autoHideDelay: 5000,
        });
        this.removeId = null;
        this.retrieveAllBoutiques();
        this.closeDialog();
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public closeDialog(): void {
    (<any>this.$refs.removeEntity).hide();
  }
}
