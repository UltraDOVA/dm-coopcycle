import { Component, Vue, Inject } from 'vue-property-decorator';

import { IBoutique } from '@/shared/model/boutique.model';
import BoutiqueService from './boutique.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class BoutiqueDetails extends Vue {
  @Inject('boutiqueService') private boutiqueService: () => BoutiqueService;
  @Inject('alertService') private alertService: () => AlertService;

  public boutique: IBoutique = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.boutiqueId) {
        vm.retrieveBoutique(to.params.boutiqueId);
      }
    });
  }

  public retrieveBoutique(boutiqueId) {
    this.boutiqueService()
      .find(boutiqueId)
      .then(res => {
        this.boutique = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
