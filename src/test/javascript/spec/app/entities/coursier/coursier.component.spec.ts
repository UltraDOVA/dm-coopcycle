/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import { ToastPlugin } from 'bootstrap-vue';

import * as config from '@/shared/config/config';
import CoursierComponent from '@/entities/coursier/coursier.vue';
import CoursierClass from '@/entities/coursier/coursier.component';
import CoursierService from '@/entities/coursier/coursier.service';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();
localVue.use(ToastPlugin);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('b-badge', {});
localVue.directive('b-modal', {});
localVue.component('b-button', {});
localVue.component('router-link', {});

const bModalStub = {
  render: () => {},
  methods: {
    hide: () => {},
    show: () => {},
  },
};

describe('Component Tests', () => {
  describe('Coursier Management Component', () => {
    let wrapper: Wrapper<CoursierClass>;
    let comp: CoursierClass;
    let coursierServiceStub: SinonStubbedInstance<CoursierService>;

    beforeEach(() => {
      coursierServiceStub = sinon.createStubInstance<CoursierService>(CoursierService);
      coursierServiceStub.retrieve.resolves({ headers: {} });

      wrapper = shallowMount<CoursierClass>(CoursierComponent, {
        store,
        i18n,
        localVue,
        stubs: { bModal: bModalStub as any },
        provide: {
          coursierService: () => coursierServiceStub,
          alertService: () => new AlertService(),
        },
      });
      comp = wrapper.vm;
    });

    it('Should call load all on init', async () => {
      // GIVEN
      coursierServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });

      // WHEN
      comp.retrieveAllCoursiers();
      await comp.$nextTick();

      // THEN
      expect(coursierServiceStub.retrieve.called).toBeTruthy();
      expect(comp.coursiers[0]).toEqual(expect.objectContaining({ id: 123 }));
    });
    it('Should call delete service on confirmDelete', async () => {
      // GIVEN
      coursierServiceStub.delete.resolves({});

      // WHEN
      comp.prepareRemove({ id: 123 });
      expect(coursierServiceStub.retrieve.callCount).toEqual(1);

      comp.removeCoursier();
      await comp.$nextTick();

      // THEN
      expect(coursierServiceStub.delete.called).toBeTruthy();
      expect(coursierServiceStub.retrieve.callCount).toEqual(2);
    });
  });
});
