/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import VueRouter from 'vue-router';

import * as config from '@/shared/config/config';
import CoursierDetailComponent from '@/entities/coursier/coursier-details.vue';
import CoursierClass from '@/entities/coursier/coursier-details.component';
import CoursierService from '@/entities/coursier/coursier.service';
import router from '@/router';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();
localVue.use(VueRouter);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('Coursier Management Detail Component', () => {
    let wrapper: Wrapper<CoursierClass>;
    let comp: CoursierClass;
    let coursierServiceStub: SinonStubbedInstance<CoursierService>;

    beforeEach(() => {
      coursierServiceStub = sinon.createStubInstance<CoursierService>(CoursierService);

      wrapper = shallowMount<CoursierClass>(CoursierDetailComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: { coursierService: () => coursierServiceStub, alertService: () => new AlertService() },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundCoursier = { id: 123 };
        coursierServiceStub.find.resolves(foundCoursier);

        // WHEN
        comp.retrieveCoursier(123);
        await comp.$nextTick();

        // THEN
        expect(comp.coursier).toBe(foundCoursier);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundCoursier = { id: 123 };
        coursierServiceStub.find.resolves(foundCoursier);

        // WHEN
        comp.beforeRouteEnter({ params: { coursierId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.coursier).toBe(foundCoursier);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
