/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import { ToastPlugin } from 'bootstrap-vue';

import * as config from '@/shared/config/config';
import BoutiqueComponent from '@/entities/boutique/boutique.vue';
import BoutiqueClass from '@/entities/boutique/boutique.component';
import BoutiqueService from '@/entities/boutique/boutique.service';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();
localVue.use(ToastPlugin);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('b-badge', {});
localVue.directive('b-modal', {});
localVue.component('b-button', {});
localVue.component('router-link', {});

const bModalStub = {
  render: () => {},
  methods: {
    hide: () => {},
    show: () => {},
  },
};

describe('Component Tests', () => {
  describe('Boutique Management Component', () => {
    let wrapper: Wrapper<BoutiqueClass>;
    let comp: BoutiqueClass;
    let boutiqueServiceStub: SinonStubbedInstance<BoutiqueService>;

    beforeEach(() => {
      boutiqueServiceStub = sinon.createStubInstance<BoutiqueService>(BoutiqueService);
      boutiqueServiceStub.retrieve.resolves({ headers: {} });

      wrapper = shallowMount<BoutiqueClass>(BoutiqueComponent, {
        store,
        i18n,
        localVue,
        stubs: { bModal: bModalStub as any },
        provide: {
          boutiqueService: () => boutiqueServiceStub,
          alertService: () => new AlertService(),
        },
      });
      comp = wrapper.vm;
    });

    it('Should call load all on init', async () => {
      // GIVEN
      boutiqueServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });

      // WHEN
      comp.retrieveAllBoutiques();
      await comp.$nextTick();

      // THEN
      expect(boutiqueServiceStub.retrieve.called).toBeTruthy();
      expect(comp.boutiques[0]).toEqual(expect.objectContaining({ id: 123 }));
    });
    it('Should call delete service on confirmDelete', async () => {
      // GIVEN
      boutiqueServiceStub.delete.resolves({});

      // WHEN
      comp.prepareRemove({ id: 123 });
      expect(boutiqueServiceStub.retrieve.callCount).toEqual(1);

      comp.removeBoutique();
      await comp.$nextTick();

      // THEN
      expect(boutiqueServiceStub.delete.called).toBeTruthy();
      expect(boutiqueServiceStub.retrieve.callCount).toEqual(2);
    });
  });
});
