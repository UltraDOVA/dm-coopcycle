/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import VueRouter from 'vue-router';

import * as config from '@/shared/config/config';
import BoutiqueDetailComponent from '@/entities/boutique/boutique-details.vue';
import BoutiqueClass from '@/entities/boutique/boutique-details.component';
import BoutiqueService from '@/entities/boutique/boutique.service';
import router from '@/router';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();
localVue.use(VueRouter);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('Boutique Management Detail Component', () => {
    let wrapper: Wrapper<BoutiqueClass>;
    let comp: BoutiqueClass;
    let boutiqueServiceStub: SinonStubbedInstance<BoutiqueService>;

    beforeEach(() => {
      boutiqueServiceStub = sinon.createStubInstance<BoutiqueService>(BoutiqueService);

      wrapper = shallowMount<BoutiqueClass>(BoutiqueDetailComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: { boutiqueService: () => boutiqueServiceStub, alertService: () => new AlertService() },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundBoutique = { id: 123 };
        boutiqueServiceStub.find.resolves(foundBoutique);

        // WHEN
        comp.retrieveBoutique(123);
        await comp.$nextTick();

        // THEN
        expect(comp.boutique).toBe(foundBoutique);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundBoutique = { id: 123 };
        boutiqueServiceStub.find.resolves(foundBoutique);

        // WHEN
        comp.beforeRouteEnter({ params: { boutiqueId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.boutique).toBe(foundBoutique);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
