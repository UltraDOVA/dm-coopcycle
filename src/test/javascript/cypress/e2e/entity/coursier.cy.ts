import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('Coursier e2e test', () => {
  const coursierPageUrl = '/coursier';
  const coursierPageUrlPattern = new RegExp('/coursier(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'user';
  const password = Cypress.env('E2E_PASSWORD') ?? 'user';
  const coursierSample = {};

  let coursier;

  beforeEach(() => {
    cy.login(username, password);
  });

  beforeEach(() => {
    cy.intercept('GET', '/api/coursiers+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/api/coursiers').as('postEntityRequest');
    cy.intercept('DELETE', '/api/coursiers/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (coursier) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/api/coursiers/${coursier.id}`,
      }).then(() => {
        coursier = undefined;
      });
    }
  });

  it('Coursiers menu should load Coursiers page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('coursier');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('Coursier').should('exist');
    cy.url().should('match', coursierPageUrlPattern);
  });

  describe('Coursier page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(coursierPageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create Coursier page', () => {
        cy.get(entityCreateButtonSelector).click();
        cy.url().should('match', new RegExp('/coursier/new$'));
        cy.getEntityCreateUpdateHeading('Coursier');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', coursierPageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/api/coursiers',
          body: coursierSample,
        }).then(({ body }) => {
          coursier = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/api/coursiers+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              body: [coursier],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(coursierPageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details Coursier page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('coursier');
        cy.get(entityDetailsBackButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', coursierPageUrlPattern);
      });

      it('edit button click should load edit Coursier page and go back', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('Coursier');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', coursierPageUrlPattern);
      });

      it('edit button click should load edit Coursier page and save', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('Coursier');
        cy.get(entityCreateSaveButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', coursierPageUrlPattern);
      });

      it('last delete button click should delete instance of Coursier', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('coursier').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click();
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', coursierPageUrlPattern);

        coursier = undefined;
      });
    });
  });

  describe('new Coursier page', () => {
    beforeEach(() => {
      cy.visit(`${coursierPageUrl}`);
      cy.get(entityCreateButtonSelector).click();
      cy.getEntityCreateUpdateHeading('Coursier');
    });

    it('should create an instance of Coursier', () => {
      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response.statusCode).to.equal(201);
        coursier = response.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response.statusCode).to.equal(200);
      });
      cy.url().should('match', coursierPageUrlPattern);
    });
  });
});
