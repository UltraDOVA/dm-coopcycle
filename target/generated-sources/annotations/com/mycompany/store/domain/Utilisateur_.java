package com.mycompany.store.domain;

import com.mycompany.store.domain.enumeration.Role;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Utilisateur.class)
public abstract class Utilisateur_ {

	public static volatile SingularAttribute<Utilisateur, Role> role;
	public static volatile SetAttribute<Utilisateur, Boutique> boutiques;
	public static volatile SingularAttribute<Utilisateur, String> mail;
	public static volatile SetAttribute<Utilisateur, Cooperative> cooperatives;
	public static volatile SetAttribute<Utilisateur, Panier> paniers;
	public static volatile SetAttribute<Utilisateur, Coursier> coursiers;
	public static volatile SingularAttribute<Utilisateur, String> tel;
	public static volatile SingularAttribute<Utilisateur, Long> id;
	public static volatile SingularAttribute<Utilisateur, String> nom;

	public static final String ROLE = "role";
	public static final String BOUTIQUES = "boutiques";
	public static final String MAIL = "mail";
	public static final String COOPERATIVES = "cooperatives";
	public static final String PANIERS = "paniers";
	public static final String COURSIERS = "coursiers";
	public static final String TEL = "tel";
	public static final String ID = "id";
	public static final String NOM = "nom";

}

