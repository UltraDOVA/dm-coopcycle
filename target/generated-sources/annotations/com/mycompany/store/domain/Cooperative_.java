package com.mycompany.store.domain;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Cooperative.class)
public abstract class Cooperative_ {

	public static volatile SetAttribute<Cooperative, Utilisateur> utilisateurs;
	public static volatile SetAttribute<Cooperative, Boutique> boutiques;
	public static volatile SingularAttribute<Cooperative, String> utilisateur;
	public static volatile SingularAttribute<Cooperative, String> directeurGeneral;
	public static volatile SingularAttribute<Cooperative, String> name;
	public static volatile SingularAttribute<Cooperative, String> societaire;
	public static volatile SingularAttribute<Cooperative, Long> id;
	public static volatile SingularAttribute<Cooperative, String> cA;

	public static final String UTILISATEURS = "utilisateurs";
	public static final String BOUTIQUES = "boutiques";
	public static final String UTILISATEUR = "utilisateur";
	public static final String DIRECTEUR_GENERAL = "directeurGeneral";
	public static final String NAME = "name";
	public static final String SOCIETAIRE = "societaire";
	public static final String ID = "id";
	public static final String C_A = "cA";

}

