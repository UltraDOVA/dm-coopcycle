package com.mycompany.store.domain;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Boutique.class)
public abstract class Boutique_ {

	public static volatile SetAttribute<Boutique, Produit> produits;
	public static volatile SetAttribute<Boutique, Cooperative> cooperatives;
	public static volatile SingularAttribute<Boutique, Utilisateur> utilisateur;
	public static volatile SingularAttribute<Boutique, String> name;
	public static volatile SingularAttribute<Boutique, Long> id;

	public static final String PRODUITS = "produits";
	public static final String COOPERATIVES = "cooperatives";
	public static final String UTILISATEUR = "utilisateur";
	public static final String NAME = "name";
	public static final String ID = "id";

}

