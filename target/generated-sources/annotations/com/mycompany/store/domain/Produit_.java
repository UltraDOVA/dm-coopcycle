package com.mycompany.store.domain;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Produit.class)
public abstract class Produit_ {

	public static volatile SingularAttribute<Produit, Boutique> boutique;
	public static volatile SetAttribute<Produit, Panier> paniers;
	public static volatile SingularAttribute<Produit, String> name;
	public static volatile SingularAttribute<Produit, Long> id;

	public static final String BOUTIQUE = "boutique";
	public static final String PANIERS = "paniers";
	public static final String NAME = "name";
	public static final String ID = "id";

}

