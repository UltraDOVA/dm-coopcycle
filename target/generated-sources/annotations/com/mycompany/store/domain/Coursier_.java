package com.mycompany.store.domain;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Coursier.class)
public abstract class Coursier_ {

	public static volatile SingularAttribute<Coursier, Panier> panier;
	public static volatile SingularAttribute<Coursier, Utilisateur> utilisateur;
	public static volatile SingularAttribute<Coursier, Long> id;

	public static final String PANIER = "panier";
	public static final String UTILISATEUR = "utilisateur";
	public static final String ID = "id";

}

