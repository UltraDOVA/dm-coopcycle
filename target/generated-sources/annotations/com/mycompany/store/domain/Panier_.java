package com.mycompany.store.domain;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Panier.class)
public abstract class Panier_ {

	public static volatile SetAttribute<Panier, Produit> produits;
	public static volatile SingularAttribute<Panier, Utilisateur> utilisateur;
	public static volatile SetAttribute<Panier, Coursier> coursiers;
	public static volatile SingularAttribute<Panier, Long> id;

	public static final String PRODUITS = "produits";
	public static final String UTILISATEUR = "utilisateur";
	public static final String COURSIERS = "coursiers";
	public static final String ID = "id";

}

